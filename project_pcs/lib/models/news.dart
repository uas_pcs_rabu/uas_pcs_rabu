class News {
  final String title;
  final String link;
  final String contentSnippet;
  final String isoDate;
  final String image;

  News({
    required this.title,
    required this.isoDate,
    required this.link,
    required this.contentSnippet,
    required this.image,
  });

  factory News.fromJson(Map<String, dynamic> json) {
  return News(
    title: json['title'],
    isoDate: json['isoDate'],
    link: json['link'],
    contentSnippet: json['contentSnippet'],
    image: json['image']['small'],
  );
}
}
