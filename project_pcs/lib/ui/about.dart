import 'package:flutter/material.dart';

class AboutPage extends StatelessWidget {
  const AboutPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About Me'),
      ),
      body: Container(
        child: ListTile(
          title: Text('Putri Aulia'),
          subtitle: Text('Editor'),
        ),
      ),
    );
  }
}
