import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class HomePage extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<HomePage> {
  List _posts = [];
  @override
  void initState() {
    super.initState();
    _getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Cuaca Terbaru')),
        body: RefreshIndicator(
            onRefresh: _handleRefresh,
            child: ListView.builder(
                itemCount: _posts.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text('${_posts[index]['jamCuaca']}',
                        maxLines: 2, overflow: TextOverflow.ellipsis),
                    subtitle: Text(
                      '${_posts[index]['cuaca']}',
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    onTap: () {},
                  );
                })));
  }

  Future<void> _handleRefresh() async {
    await _getData();
  }

  Future _getData() async {
    try {
      final response = await http.get(
          Uri.parse('https://ibnux.github.io/BMKG-importer/cuaca/501568.json'));
      if (response.statusCode == 200) {
        final data = json.decode(response.body);
        setState(() {
          _posts = data['data'];
        });
      }
    } catch (e) {
      print(e);
    }
  }
}
