import 'package:flutter/material.dart';

class ContactPage extends StatelessWidget {
  const ContactPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contact'),
      ),
      body: Container(
        child: ListTile(
          title: Text('Putri Aulia'),
          subtitle: Text('Editor'),
        ),
      ),
    );
  }
}
