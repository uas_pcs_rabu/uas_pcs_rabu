import 'package:flutter/material.dart';
import 'package:project_pcs/models/weather.dart';
import 'package:project_pcs/resources/weather_api.dart';

class TaskPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Data Cuaca'),
      ),
      body: FutureBuilder<List<Weather>>(
        future: fetchDailyWeather(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasError) {
            return Center(
              child: Text('Error: ${snapshot.error}'),
            );
          } else {
            List<Weather>? weatherList = snapshot.data;
            return ListView.builder(
              itemCount: weatherList?.length,
              itemBuilder: (context, index) {
                Weather weather = weatherList![index];
                return ListTile(
                  title: Text(weather.jamCuaca),
                  subtitle: Text(weather.cuaca),
                );
              },
            );
          }
        },
      ),
    );
  }
}
